﻿using ServiceStack;
using servicestack_docker_helloworld.ServiceModel;

namespace servicestack_docker_helloworld.ServiceInterface
{
    public class MyServices : Service
    {
        public object Any(Hello request)
        {
            return new HelloResponse { Result = "Hello, {0}!".Fmt(request.Name) };
        }
    }
}