# ServiceStack Nginx Mono_fastcgi Host
#
# VERSION 0.01

FROM    debian:wheezy
MAINTAINER  Jürgen Happe <juergen.happe@immonet.de>

RUN apt-get update && apt-get install -y apache2 ca-certificates git inotify-tools nginx openssh-server wget
# add key and mono sources
RUN wget -qO - http://download.mono-project.com/repo/xamarin.gpg | apt-key add - \s
RUN echo "deb http://download.mono-project.com/repo/debian wheezy main" | tee /etc/apt/sources.list.d/mono-xamarin.list
# install new mono version
RUN apt-get update && apt-get install -y mono-complete mono-fastcgi-server
RUN mkdir publish && mkdir source && mkdir nuget

# Download nuget
WORKDIR /nuget
RUN wget https://dist.nuget.org/win-x86-commandline/v3.4.4/NuGet.exe

WORKDIR /source
COPY ./ /source/
RUN ls -la  /source/*
#RUN git clone https://bitbucket.org/rooks/servicestack-docker-helloworld.git /source/servicestack-docker-helloworld
RUN mkdir packages
RUN mono /nuget/NuGet.exe install /source/servicestack-docker-helloworld/servicestack_docker_helloworld/packages.config  -o /source/packages/
RUN xbuild servicestack-docker-helloworld.sln /t:Rebuild /p:outdir="/publish/" /p:Configuration=Release /p:Platform="Any CPU"


RUN cp  /source/config/fastcgi_params /etc/nginx/fastcgi_params
RUN cp  /source/config/default /etc/nginx/sites-available/default
RUN update-rc.d nginx defaults

EXPOSE 80
CMD /usr/sbin/service nginx start && /usr/bin/fastcgi-mono-server /applications=/:/publish/_PublishedWebsites/servicestack_docker_helloworld/  /socket=tcp:127.0.0.1:9000 /logfile=/var/log/mono/fastcgi.log /printlog=True

